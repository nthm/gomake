import React from 'react'
import Link from 'next/link'

import redirect from '../lib/redirect'
import checkLoggedIn from '../lib/checkLoggedIn'

import LoginBox from '../components/LoginBox'

export default class Login extends React.Component {
  static async getInitialProps (context) {
    const { loggedInUser } = await checkLoggedIn(context.apolloClient)

    if (loggedInUser.user) {
      // Already signed in? No need to continue.
      // Throw them back to the main page
      redirect(context, '/')
    }

    return {}
  }

  render () {
    return (
      <React.Fragment>
        {/* LoginBox handles all login logic. */}
        <LoginBox />
        <hr />
        New? <Link prefetch href='/register'><a>Register</a></Link>
      </React.Fragment>
    )
  }
};
