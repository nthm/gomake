# WIP: Next.js/Apollo/TailwindCSS

This is a clone and merge of two examples from _next.js/examples/_:

- `with-apollo-auth`
- `with-tailwindcss`

GraphQL backend is: https://api.graph.cool/simple/v1/cj5geu3slxl7t0127y8sity9r
