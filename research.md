# Researching stacks

Need a GraphQL server. Will use Apollo since it's largely plug and play. There's
the option to use A GraphQL BaaS but we'll opt for a local install.

- [GitHub - graphql/graphiql: An in-browser IDE for exploring GraphQL.](https://github.com/graphql/graphiql)
- [Generating a schema | GraphQL Tools](https://www.apollographql.com/docs/graphql-tools/generate-schema.html)

The underlying database is largely abstracted when using GraphQL, but there's
not much documentation on how to setup a server. Most docs just talk about
querying existing servers.

It's possible to use sqlite3 which is nice because the entire database is just a
file. Keep installs simple. I'll likely commit a small database to the repo.
There are abstraction layers like Knex and Sequelize (an ORM I've used before)
but they may just be another thing to learn and could be avoided.

- [GitHub - mapbox/node-sqlite3: Asynchronous, non-blocking SQLite3 bindings for Node.js](https://github.com/mapbox/node-sqlite3)
- [Knex.js - A SQL Query Builder for Javascript](https://knexjs.org/#faq)

This will be a Next.js application and will be deployed through Now.sh. Next.js
has a repository of example starters which is great:

- [Examples · zeit/next.js · GitHub](https://github.com/zeit/next.js/tree/canary/examples)

However, there's no standard for authentication/authorization. Ooth looks
promising; more research is needed.

- [Ooth — user accounts for node.js – The Ideal System – Medium](https://medium.com/the-ideal-system/ooth-user-accounts-for-node-js-93cfcd28ed1a)
- [Ooth - a user identity management system | ooth](https://nmaro.github.io/ooth/)
- [Authentication | Apollo Client](https://www.apollographql.com/docs/react/recipes/authentication.html)
- [User authentication with next.js (and Apollo GraphQL)](https://medium.com/the-ideal-system/user-authentication-with-next-js-and-apollo-graphql-aa5ac38e6f43)

Methods for priming the database with some example data will be needed:

- [graphql-express-sqlite/insertUsers.js at master · mrblueblue/graphql-express-sqlite · GitHub](https://github.com/mrblueblue/graphql-express-sqlite/blob/master/database/methods/insertUsers.js)
- [GraphQL and MongoDB — a quick example – The Ideal System – Medium](https://medium.com/the-ideal-system/graphql-and-mongodb-a-quick-example-34643e637e49)
- [GitHub - nmaro/graphql-mongodb-example](https://github.com/nmaro/graphql-mongodb-example)

The _Apollo Univeral Starter Kit_ is huge, and might be too much time to
understand before any real code can be written. It supports Android/iOS apps out
of the box and those require their own frontend code.

- [GitHub - sysgears/apollo-universal-starter-kit](https://github.com/sysgears/apollo-universal-starter-kit)

Staart might be a good starter. Made by the guy behind Ooth and aims to be a
starter with Ooth+React to have user login, registration, verify email, forgot
password, etc.

- [Dashboard](https://staart.nmr.io/dashboard)
- [GitHub - nmaro/staart](https://github.com/nmaro/staart)

I already know the application will use Tailwind CSS. That needs to be
integrated into the build system to use PostCSS plugins like PurgeCSS (but
later, way later)

---

# Staart

Using `staart` over `next-auth` because it's a more complete example. The code
is largely split between an example repo and the "library". It's not a library
though, because the components it supplies are small enough to copy/paste.

The project is made of three microservices and two databases.

The UI is a Next app that's wrapped in an Express server (for seemingly no
reason?). All requests for data go to the API server which is _seperate_. They
actually have it as another container entirely. Lastly an ooth server handles
authentication. The three servers listen to ports 3000, 3001, 3002, and then
there's an Express proxy on 8080 that ties them together under different URL
paths. Nice. All servers talk to a Redis server that manages Express sessions.
The API server talks to MongoDB.

You need to have Redis to have sessions at all.

I want as few services running as possible. I don't want microservices because
it means more for someone else to understand, and I definitely don't want other
non-node services like Mongo or Redis. Not to mention Redis' license.

It's tempting to just drop microservices and use the in-memory Express session
store. This would limit future scale _and_ means everyone needs to relogin on
each reboot. Been there done that. The business people on this project know SQL,
so I'll use `connect-sqlite3` over the usual `connect-redis` - yes, I understand
the overhead on the database, it'll live.

The more I read into merging microservices it seems ooth might already be a lot.
Its backend, MongoDB, is only 60 lines of TS and could be ported to SQLite3, but
it means asking if Passport.js would be more simple.

Either way a strategy will need to be created for Ooth or Passport.js

- [Breakdown of Staart](https://nmaro.github.io/ooth/next.html)
- [SQLite3 session store](https://github.com/rawberg/connect-sqlite3)
