// Express wrapper for Next.js
// https://github.com/zeit/next.js/blob/canary/examples/custom-server-express/

const express = require('express')
const next = require('next')

const app = express()
const nextApp = next({
  dev: process.env.NODE_ENV !== 'production'
})
const handle = nextApp.getRequestHandler()

// TODO: Might need cookie-parser as per staart docs

(async function start() {
  // TODO: Plug in ooth and the GraphQL playground here

  await nextApp.prepare()
  app.use(handle)

  const port = process.env.PORT || 3000
  await app.listen(port)
  console.log(`Express online at ${port}`)
})()
